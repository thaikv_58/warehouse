package controllers;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import com.avaje.ebean.annotation.SqlSelect;
import models.Product;
import models.StockItem;
import models.Tag;
import play.db.ebean.Model;
import play.mvc.Result;
import play.mvc.Controller;
import play.mvc.Security;
import views.html.products.list;
import play.data.Form;
import views.html.products.details;

@Security.Authenticated(Secured.class)
public class Products extends Controller {
    private static final Form<Product> productForm = Form.form(Product.class);

    public static Result list(Integer page) {
        Page<Product> products = Product.find(page);
        return ok(views.html.catalog.render(products));
    }

    public static Result newProduct() {
        return ok(details.render(productForm));
    }

    public static Result save() {
        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }

        //... (binding and error handling)
        Product product = boundForm.get();
        if (product.id == null) {
            List<Product> products = Product.findAll();
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).ean.equals(product.ean)) {
                    flash("error", "The ean is same.");
                    return badRequest(details.render(boundForm));
                }
            }
        }

        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags) {
            if (tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;
        //... (success message and redirect)
        //product.save();
        //Ebean.save(product);

        if (product.id == null) {
            product.save();
            //Ebean.save(product);
        } else {
            product.update();
        }

        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
        stockItem.save();

        flash("success", String.format("Successfully added product %s", product));
        return redirect(routes.Products.list(0));
    }

    public static Result details(Product product) {
        if (product == null) {
            return notFound(String.format("Product %s does not exist.", product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
        //return TODO; // show a products edit form
    }

    public static Result delete(String ean) {
        final Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exists.", ean));
        }
        //Product.remove(product);

        for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
        product.delete();
        return redirect(routes.Products.list(0));
    }

    public static Model.Finder<Long, Product> find = new Model.Finder<>(Long.class, Product.class);



}