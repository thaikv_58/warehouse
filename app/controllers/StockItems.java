package controllers;

import play.mvc.Controller;
import java.util.List;
import models.StockItem;
import javax.xml.transform.Result;

/**
 * Created by KieuVan on 13/03/2015.
 */
public class StockItems extends Controller {
    public static play.mvc.Result index() {
        List<StockItem> items = StockItem.find.where().ge("quantity", 300).orderBy("quantity desc").setMaxRows(5).findList();
        return ok(items.toString());
    //return TODO;
    }
}
