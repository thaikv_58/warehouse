package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
@Entity
public class Product extends Model implements PathBindable<Product>{
    private static List<Product> products;
	static {
		products = new ArrayList<Product>();
		products.add(new Product("1111111111111", "Paperclips 1",
			"Paperclips description 1"));
		products.add(new Product("2222222222222", "Paperclips 2",
			"Paperclips description 2"));
		products.add(new Product("3333333333333", "Paperclips 3",
			"Paperclips description 3"));
		products.add(new Product("4444444444444", "Paperclips 4",
			"Paperclips description 4"));
		products.add(new Product("5555555555555", "Paperclips 5",
			"Paperclips description 5"));
	}

	@Id
	public Long id;
	@Constraints.Required
	public String ean;
	@Constraints.Required
	public String name;
	@Constraints.Required
	public String description;
	public byte[] picture;
    public Date date;

    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;
	//public List<Tag> tags;    // trường quan hệ nối với Tag
    public static Finder<Long,Product> find = new Finder<Long,Product>(Long.class, Product.class);

    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();
	public Product() {}
	public Product(String ean, String name, String description) {
		this.ean = ean;
		this.name = name;
		this.description = description;
	}
	public String toString () {
		return String.format("%s - %s", ean, name);
	}
	
	public static List<Product> findAll() {
        return find.all();
        //return new ArrayList<Product>(products);
	}
 
	public static Product findByEan(String ean) {
		/*
        for (Product candidate : products) {
			if (candidate.ean.equals(ean)) {       
				return candidate;
			}
		}
		return null;
		*/
        return find.where().eq("ean", ean).findUnique();
	}
 
	public static List<Product> findByName(String term) {
		final List<Product> results = new ArrayList<Product>();
		for (Product candidate : products) {
			if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
				results.add(candidate);
			}
		}
		return results;
	}

	//public static boolean remove(Product product) {
	//	return products.remove(product);
	//}

    public static Page<Product> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(3)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }

	@Override
	public Product bind(String key, String value) {
		return findByEan(value);
	}

	@Override
	public String unbind(String key) {
		return ean;
	}

	@Override
	public String javascriptUnbind() {
		return ean;
	}
}