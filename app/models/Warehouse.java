package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.ArrayList;

@Entity
public class Warehouse extends Model {
    @Id
    public Long Id;
    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();
    public String toString() {
        return name;
    }
    public String name;
    @OneToOne
    public  Address address;
    public static Finder<Long, Warehouse> find = new Finder<>(Long.class, Warehouse.class);
}