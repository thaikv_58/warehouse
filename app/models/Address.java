package models;
import models.Warehouse;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Address {
    @javax.persistence.Id
    public Long Id;
    @OneToOne(mappedBy = "address")
    public Warehouse warehouse;
    public String street;
    public String number;
    public String postalCode;
    public String city;
    public String country;
}